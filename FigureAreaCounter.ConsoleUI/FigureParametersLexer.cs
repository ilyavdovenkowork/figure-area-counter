﻿namespace FigureAreaCounter.ConsoleUI;

public class FigureParametersLexer
{
    private readonly Dictionary<string, Func<IEnumerable<string>>> _figureUserInputBuilders;

    public FigureParametersLexer(IDictionary<string, Func<IEnumerable<string>>> figureParametersBuilders)
    {
        _figureUserInputBuilders = new Dictionary<string, Func<IEnumerable<string>>>(figureParametersBuilders);
    }

    public IEnumerable<string> ParseFigureParameters(string figureType)
    {
        if (_figureUserInputBuilders.TryGetValue(figureType, out var figureParametersParseFunction))
        {
            return figureParametersParseFunction.Invoke();
        }

        throw new ArgumentException();
    }
}