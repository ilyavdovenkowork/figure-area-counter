﻿using FigureAreaCounter.Core.Models.Parameters.Interfaces;

namespace FigureAreaCounter.ConsoleUI;

public class FigureParametersParser
{
    private readonly Dictionary<string, Func<IEnumerable<string>, IFigureParameters>> _figureParametersBuilders;

    public FigureParametersParser(IDictionary<string, Func<IEnumerable<string>, IFigureParameters>> figureParametersBuilders)
    {
        _figureParametersBuilders = new Dictionary<string, Func<IEnumerable<string>, IFigureParameters>>(figureParametersBuilders);
    }

    public IFigureParameters ParseFigureParameters(string figureType, IEnumerable<string> userInput)
    {
        if (_figureParametersBuilders.TryGetValue(figureType, out var figureParametersParseFunction))
        {
            return figureParametersParseFunction.Invoke(userInput);
        }

        throw new ArgumentException();
    }
}