using FigureAreaCounter.ConsoleUI;
using FigureAreaCounter.Core.Models.Parameters;
using FigureAreaCounter.Core.Models.Parameters.Interfaces;
using FigureAreaCounter.Core.Services;
using FigureAreaCounter.Core.Services.Factories;

const string circleName = "circle";
const string triangleName = "triangle";
const string rectangleName = "rectangle";

var calculator = new FigureAreaCalculator();
var factory = new FigureFactory();

var lexer = new FigureParametersLexer(new Dictionary<string, Func<IEnumerable<string>>>()
{
    { circleName, GetCircleUserInput },
    { triangleName, GetTriangleUserInput },
    { rectangleName, GetRectangleUserInput }
});

var parser = new FigureParametersParser(new Dictionary<string, Func<IEnumerable<string>, IFigureParameters>>
{
    { circleName, GetCircleParameters },
    { triangleName, GetTriangleParameters },
    { rectangleName, GetRectangleParameters }
});

try
{
    while (true)
    {
        Console.WriteLine("Enter figure type (circle/triangle/rectangle) or 'quit' to exit:");
        var userInput = Console.ReadLine();

        if (userInput == null)
        {
            throw new ArgumentNullException(nameof(userInput), "User input cannot be null.");
        }

        if (userInput.ToLower() == "quit")
        {
            break;
        }

        try
        {
            var userTokens = lexer.ParseFigureParameters(userInput);
            var parameters = parser.ParseFigureParameters(userInput.ToLower(), userTokens);
            var figure = factory.CreateFigure(userInput, parameters);
            Console.WriteLine($"Area of {figure.GetType().Name} is {calculator.CalculateArea(figure)} in SI.");
        }
        catch (InvalidOperationException ex)
        {
            Console.WriteLine(ex.Message);
        }
        catch (ArgumentException ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}
catch (Exception e)
{
    Console.WriteLine(e.Message);
    Environment.Exit(-1);
}


static IEnumerable<string> GetTriangleUserInput()
{
    Console.WriteLine("Enter triangle side A:");
    yield return Console.ReadLine()
                 ?? throw new InvalidOperationException("Empty user input on triangle side A.");

    Console.WriteLine("Enter triangle side B:");
    yield return Console.ReadLine()
                 ?? throw new InvalidOperationException("Empty user input on triangle side B.");

    Console.WriteLine("Enter triangle side C:");
    yield return Console.ReadLine()
                 ?? throw new InvalidOperationException("Empty user input on triangle side C.");
}

static IEnumerable<string> GetRectangleUserInput()
{
    Console.WriteLine("Enter rectangle width:");
    yield return Console.ReadLine()
                 ?? throw new InvalidOperationException("Empty user input on rect width.");

    Console.WriteLine("Enter rectangle height:");
    yield return Console.ReadLine()
                 ?? throw new InvalidOperationException("Empty user input on rect height.");
}

static IEnumerable<string> GetCircleUserInput()
{
    Console.WriteLine("Enter circle radius:");
    yield return Console.ReadLine()
                 ?? throw new InvalidOperationException("Empty user input on circle radius.");
}

static IFigureParameters GetCircleParameters(IEnumerable<string> userInput)
{
    if (double.TryParse(userInput.First(), out var radius))
    {
        return new CircleParameters()
        {
            Radius = radius
        };
    }

    throw new ArgumentException("Can not parse circle parameters.", nameof(userInput));
}

static IFigureParameters GetTriangleParameters(IEnumerable<string> userInput)
{
    var userInputList = userInput.ToList();
    if (double.TryParse(userInputList[0], out var sideA)
        && double.TryParse(userInputList[1], out var sideB)
        && double.TryParse(userInputList[2], out var sideC))
    {
        return new TriangleParameters()
        {
            A = sideA,
            B = sideB,
            C = sideC
        };
    }

    throw new ArgumentException("Can not parse triangle parameters.", nameof(userInput));
}

static IFigureParameters GetRectangleParameters(IEnumerable<string> userInput)
{
    var userInputList = userInput.ToList();
    if (double.TryParse(userInputList[0], out var width)
        && double.TryParse(userInputList[1], out var height))
    {
        return new RectangleParameters()
        {
            Width = width,
            Height = height
        };
    }

    throw new ArgumentException("Can not parse rectangle parameters.", nameof(userInput));
}