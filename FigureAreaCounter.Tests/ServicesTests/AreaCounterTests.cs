﻿using FigureAreaCounter.Core.Models.Figures;
using FigureAreaCounter.Core.Models.Figures.Interfaces;
using FigureAreaCounter.Core.Services;

namespace FigureAreaCounter.Tests.ServicesTests;

[TestClass]
public class AreaCounterTests
{
    [TestMethod]
    public void TestCalculateCircleArea()
    {
        var circle = new Circle { Radius = 5 };
        var areaCounter = new FigureAreaCalculator();
        var area = areaCounter.CalculateArea(circle);
        Assert.AreEqual(78.539816339744831, area, 0.0001);
    }

    [TestMethod]
    public void TestCalculateRectangleArea()
    {
        var rectangle = new Rectangle { Width = 10, Height = 5 };
        var areaCounter = new FigureAreaCalculator();
        var area = areaCounter.CalculateArea(rectangle);
        Assert.AreEqual(50, area, 0.0001);
    }

    [TestMethod]
    public void TestCalculateTriangleArea()
    {
        var triangle = new Triangle { A = 3, B = 4, C = 5 };
        var areaCounter = new FigureAreaCalculator();
        var area = areaCounter.CalculateArea(triangle);
        Assert.AreEqual(6, area, 0.0001);
    }

    [TestMethod]
    public void TestCalculateRightTriangleArea()
    {
        var rightTriangle = new RightTriangle(5, 3, 4);
        var areaCounter = new FigureAreaCalculator();
        var area = areaCounter.CalculateArea(rightTriangle);
        Assert.AreEqual(6, area, 0.0001);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentNullException))]
    public void TestCalculateAreaWithNullFigure()
    {
        var areaCounter = new FigureAreaCalculator();
        areaCounter.CalculateArea(null);
    }
    
    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCalculateAreaWithNonExistingFigure()
    {
        var areaCounter = new FigureAreaCalculator();
        areaCounter.CalculateArea(new UnsupportedFigure());
    }
    
    private class UnsupportedFigure: IFigure
    {
        public string Name { get; }
    }
}