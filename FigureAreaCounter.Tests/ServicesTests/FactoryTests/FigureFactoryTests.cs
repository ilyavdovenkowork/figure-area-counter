﻿using FigureAreaCounter.Core.Models.Figures;
using FigureAreaCounter.Core.Models.Parameters;
using FigureAreaCounter.Core.Services.Factories;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.HandlerModels;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.Interfaces;

namespace FigureAreaCounter.Tests.ServicesTests.FactoryTests;

[TestClass]
public class FigureFactoryTests
{
    [TestMethod]
    public void CreateFigure_Circle_ValidParameters_ReturnsCircle()
    {
        // Arrange
        var factory = new FigureFactory();
        var parameters = new CircleParameters { Radius = 5 };

        // Act
        var result = factory.CreateFigure("circle", parameters);

        // Assert
        Assert.IsInstanceOfType(result, typeof(Circle));
        var circle = (Circle)result;
        Assert.AreEqual(5, circle.Radius);
    }

    [TestMethod]
    public void CreateFigure_Rectangle_ValidParameters_ReturnsRectangle()
    {
        // Arrange
        var factory = new FigureFactory();
        var parameters = new RectangleParameters { Width = 5, Height = 10 };

        // Act
        var result = factory.CreateFigure("rectangle", parameters);

        // Assert
        Assert.IsInstanceOfType(result, typeof(Rectangle));
        var rectangle = (Rectangle)result;
        Assert.AreEqual(5, rectangle.Width);
        Assert.AreEqual(10, rectangle.Height);
    }

    [TestMethod]
    public void CreateFigure_Triangle_ValidParameters_ReturnsTriangle()
    {
        // Arrange
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 3, B = 4, C = 5 };

        // Act
        var result = factory.CreateFigure("triangle", parameters);

        // Assert
        Assert.IsInstanceOfType(result, typeof(Triangle));
        var triangle = (Triangle)result;
        Assert.AreEqual(3, triangle.A);
        Assert.AreEqual(4, triangle.B);
        Assert.AreEqual(5, triangle.C);
    }

    [TestMethod]
    public void CreateFigure_RightTriangle_ValidParameters_ReturnsRightTriangle()
    {
        // Arrange
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 3, B = 4, C = 5 };

        // Act
        var result = factory.CreateFigure("triangle", parameters);

        // Assert
        Assert.IsInstanceOfType(result, typeof(RightTriangle));
        var rightTriangle = (RightTriangle)result;
        Assert.AreEqual(3, rightTriangle.A);
        Assert.AreEqual(4, rightTriangle.B);
        Assert.AreEqual(5, rightTriangle.C);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void CreateFigure_InvalidFigureType_ThrowsArgumentException()
    {
        // Arrange
        var factory = new FigureFactory();
        var parameters = new CircleParameters { Radius = 5 };

        // Act & Assert
        factory.CreateFigure("invalid", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void CreateFigure_InvalidParameters_ThrowsArgumentException()
    {
        // Arrange
        var factory = new FigureFactory();
        var parameters = new CircleParameters { };

        // Act & Assert
        factory.CreateFigure("circle", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateCircle_InvalidParameters()
    {
        var factory = new FigureFactory();
        var parameters = new CircleParameters { Radius = -1 };
        factory.CreateFigure("circle", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateRectangle_InvalidParameters()
    {
        var factory = new FigureFactory();
        var parameters = new RectangleParameters { Width = -1, Height = 10 };
        factory.CreateFigure("rectangle", parameters);
    }
    
    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateRectangle_InvalidHeightParameters()
    {
        var factory = new FigureFactory();
        var parameters = new RectangleParameters { Width = 1, Height = -10 };
        factory.CreateFigure("rectangle", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateRightTriangle_InvalidParameters()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 3, B = 4, C = 8 };
        factory.CreateFigure("triangle", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateFigure_UnknownType()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 3, B = 4, C = 5 };
        factory.CreateFigure("unknown", parameters);
    }

    [TestMethod]
    public void TestCreateTriangle_EqualSides()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 5, B = 5, C = 5 };
        var triangle = factory.CreateFigure("triangle", parameters) as Triangle;
        Assert.IsNotNull(triangle);
        Assert.AreEqual(5, triangle.A);
        Assert.AreEqual(5, triangle.B);
        Assert.AreEqual(5, triangle.C);
    }

    [TestMethod]
    public void TestCreateCircle()
    {
        var factory = new FigureFactory();
        var parameters = new CircleParameters { Radius = 5 };
        var circle = factory.CreateFigure("circle", parameters);
        Assert.AreEqual("Circle", circle.Name);
        Assert.AreEqual(5, ((Circle)circle).Radius);
    }

    [TestMethod]
    public void TestCreateRectangle()
    {
        var factory = new FigureFactory();
        var parameters = new RectangleParameters { Width = 10, Height = 5 };
        var rectangle = factory.CreateFigure("rectangle", parameters);
        Assert.AreEqual("Rectangle", rectangle.Name);
        Assert.AreEqual(10, ((Rectangle)rectangle).Width);
        Assert.AreEqual(5, ((Rectangle)rectangle).Height);
    }

    [TestMethod]
    public void TestCreateTriangle()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 3, B = 5, C = 5 };
        var triangle = factory.CreateFigure("triangle", parameters);
        Assert.AreEqual("Triangle", triangle.Name);
        Assert.AreEqual(3, ((Triangle)triangle).A);
        Assert.AreEqual(5, ((Triangle)triangle).B);
        Assert.AreEqual(5, ((Triangle)triangle).C);
    }

    [TestMethod]
    public void TestCreateRightTriangle()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 3, B = 4, C = 5 };
        var rightTriangle = factory.CreateFigure("triangle", parameters);
        Assert.AreEqual("Right Triangle", rightTriangle.Name);
        Assert.AreEqual(3, ((RightTriangle)rightTriangle).CathetusOne);
        Assert.AreEqual(4, ((RightTriangle)rightTriangle).CathetusTwo);
        Assert.AreEqual(5, ((RightTriangle)rightTriangle).Hypotenuse);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateInvalidFigure()
    {
        var factory = new FigureFactory();
        var parameters = new CircleParameters { Radius = 5 };
        factory.CreateFigure("invalid figure", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateCircleWithInvalidParameters()
    {
        var factory = new FigureFactory();
        var parameters = new RectangleParameters { Width = 10, Height = 5 };
        factory.CreateFigure("circle", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateRectangleWithInvalidParameters()
    {
        var factory = new FigureFactory();
        var parameters = new CircleParameters { Radius = 5 };
        factory.CreateFigure("rectangle", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateTriangleWithInvalidParameters()
    {
        var factory = new FigureFactory();
        var parameters = new RectangleParameters { Width = 10, Height = 5 };
        factory.CreateFigure("triangle", parameters);
    }

    [TestMethod]
    public void TestCreateRightTriangleWithInvalidParameters()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 3, B = 4, C = 6 };
        var figure = factory.CreateFigure("triangle", parameters);
        Assert.IsInstanceOfType(figure, typeof(Triangle));
    }
    
    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateRightTriangleWithNonRightTriangleParameters()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 3, B = 4, C = 7 };
        factory.CreateFigure("triangle", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateFigureWithNullParameters()
    {
        var factory = new FigureFactory();
        factory.CreateFigure("circle", null);
    }
    
    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateRightTriangleWithZeroCathetus()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 0, B = 4, C = 4 };
        factory.CreateFigure("triangle", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateRightTriangleWithNegativeCathetus()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = -3, B = 4, C = 5 };
        factory.CreateFigure("triangle", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateRightTriangleWithZeroHypotenuse()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 3, B = 4, C = 0 };
        factory.CreateFigure("triangle", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateRightTriangleWithNegativeHypotenuse()
    {
        var factory = new FigureFactory();
        var parameters = new TriangleParameters { A = 3, B = 4, C = -5 };
        factory.CreateFigure("triangle", parameters);
    }
    
    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateTriangleWithWrongParamsCount()
    {
        var factory = new FigureFactory();
        var parameters = new CircleParameters { Radius = 3,};
        factory.CreateFigure("triangle", parameters);
    }
    

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateFigureWithEmptyName()
    {
        var factory = new FigureFactory();
        var parameters = new CircleParameters { Radius = 5 };
        factory.CreateFigure("", parameters);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateFigureWithNullName()
    {
        var factory = new FigureFactory();
        var parameters = new CircleParameters { Radius = 5 };
        factory.CreateFigure(null, parameters);
    }
    
    
    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestCreateEmptyHandler()
    {
        var handler = new EmptyHandler() as IFigureHandler;
        handler.Handle(new FigureInput("circle", new CircleParameters()
        {
            Radius = 5
        }));
        
    }
    

}