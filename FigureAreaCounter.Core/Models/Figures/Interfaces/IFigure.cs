namespace FigureAreaCounter.Core.Models.Figures.Interfaces;

public interface IFigure 
{
    public string Name { get; }

}