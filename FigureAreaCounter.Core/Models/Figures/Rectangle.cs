﻿using FigureAreaCounter.Core.Models.Figures.Interfaces;

namespace FigureAreaCounter.Core.Models.Figures;

public class Rectangle : IFigure 
{
    public double Width { get; init; }
    public double Height { get; init; }
    public string Name => "Rectangle";
}