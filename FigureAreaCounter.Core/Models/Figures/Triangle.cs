using FigureAreaCounter.Core.Models.Figures.Interfaces;

namespace FigureAreaCounter.Core.Models.Figures;

public class Triangle : IFigure
{
    public Triangle()
    {
        Name = "Triangle";
    }
    
    protected Triangle(double a, double b, double c, string name)
    {
        A = a;
        B = b;
        C = c;
        Name = name;
    }
        
    public double A { get; init; }
    public double B { get; init; }
    public double C { get; init; }
    public string Name { get; init; }
    
}