using FigureAreaCounter.Core.Models.Figures.Interfaces;

namespace FigureAreaCounter.Core.Models.Figures;

public class Circle : IFigure 
{
    public double Radius { get; set; }
    public string Name => "Circle";
}

