﻿namespace FigureAreaCounter.Core.Models.Figures;

public sealed class RightTriangle : Triangle
{
    public double CathetusOne { get; }
    
    public double CathetusTwo { get; }

    public double Hypotenuse { get;  }

    public RightTriangle(double hypotenuse, double cathetusTwo, double cathetusOne) : base(cathetusOne, cathetusTwo, hypotenuse, "Right Triangle")
    {
        Hypotenuse = hypotenuse;
        CathetusTwo = cathetusTwo;
        CathetusOne = cathetusOne;
    }
}