using FigureAreaCounter.Core.Models.Parameters.Interfaces;

namespace FigureAreaCounter.Core.Models.Parameters;

public class TriangleParameters : IFigureParameters
{
    public double A { get; init; }
    public double B { get; init; }
    public double C { get; init; }
    
}