using FigureAreaCounter.Core.Models.Parameters.Interfaces;

namespace FigureAreaCounter.Core.Models.Parameters;

public class CircleParameters : IFigureParameters
{
    public double Radius { get; init; }
}