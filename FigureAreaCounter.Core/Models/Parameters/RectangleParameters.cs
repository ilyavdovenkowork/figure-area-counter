using FigureAreaCounter.Core.Models.Parameters.Interfaces;

namespace FigureAreaCounter.Core.Models.Parameters;

public class RectangleParameters : IFigureParameters 
{
    public double Width { get; init; }
    public double Height { get; init; }
}