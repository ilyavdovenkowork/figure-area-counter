﻿using FigureAreaCounter.Core.Models.Figures.Interfaces;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.HandlerModels;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.Interfaces;

namespace FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers;

public abstract class FigureHandlerBase : IFigureHandler
{
    private readonly IFigureHandler? _nextHandler;

    protected FigureHandlerBase(IFigureHandler? nextHandler)
    {
        _nextHandler = nextHandler;
    }

    public virtual IFigure Handle(FigureInput input)
    {
        if (CanHandle(input))
        {
            return HandleInternal(input);
        }

        return _nextHandler?.Handle(input)
               ?? throw new ArgumentException("Can not handle figure with this input params", nameof(input));
    }

    protected abstract bool CanHandle(FigureInput input);

    protected abstract IFigure HandleInternal(FigureInput input);
}