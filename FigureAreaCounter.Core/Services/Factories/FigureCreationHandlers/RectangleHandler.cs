﻿using FigureAreaCounter.Core.Models.Figures;
using FigureAreaCounter.Core.Models.Figures.Interfaces;
using FigureAreaCounter.Core.Models.Parameters;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.HandlerModels;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.Interfaces;

namespace FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers;

public class RectangleHandler : FigureHandlerBase
{
    public RectangleHandler(IFigureHandler? nextHandler) : base(nextHandler)
    {
    }

    protected override bool CanHandle(FigureInput input)
    {
        return input.FigureType.Equals("rectangle", StringComparison.OrdinalIgnoreCase);
    }

    protected override IFigure HandleInternal(FigureInput input)
    {
        if (input.Parameters is not RectangleParameters parameters)
        {
            throw new ArgumentException("Invalid parameters for rectangle figure");
        }

        if (parameters.Width < 0)
        {
            throw new ArgumentException("Width can not be less then 0.", nameof(input));
        }
        
        if (parameters.Height < 0)
        {
            throw new ArgumentException("Height can not be less then 0.", nameof(input));
        }

        var rectangle = new Rectangle
        {
            Width = parameters.Width,
            Height = parameters.Height
        };

        return rectangle;
    }
}