﻿using FigureAreaCounter.Core.Models.Figures;
using FigureAreaCounter.Core.Models.Figures.Interfaces;
using FigureAreaCounter.Core.Models.Parameters;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.HandlerModels;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.Interfaces;

namespace FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers;

public class RightTriangleHandler : TriangleHandler
{
    private IEnumerable<double>? _sortedSides;
    
    public RightTriangleHandler(IFigureHandler? nextHandler) : base(nextHandler)
    {
    }

    protected override bool CanHandle(FigureInput input)
    {
        if (input.Parameters is not TriangleParameters parameters || !input.FigureType.Equals("triangle", StringComparison.OrdinalIgnoreCase))
        {
            return false;
        }
        
        var sortedSides = SortSides(parameters.A, parameters.B, parameters.C).ToList();
        
        if (!IsTriangleExists(parameters.A, parameters.B, parameters.C) || !IsRightTriangle(sortedSides))
        {
            return false;
        }

        _sortedSides = sortedSides;
        return true;

    }

    protected override IFigure HandleInternal(FigureInput input)
    {
        if (input.Parameters is not TriangleParameters)
        {
            throw new ArgumentException("Invalid parameters for triangle figure", nameof(input));
        }

        if (_sortedSides == null || _sortedSides.Count() != 3)
        {
            throw new ArgumentException("Can not handle input", nameof(input));
        }

        var sortedList = _sortedSides.ToList();
        var triangle = new RightTriangle(sortedList[2], sortedList[1], sortedList[0]);
        return triangle;

    }
    
    private static IEnumerable<double> SortSides(double side1, double side2, double side3)
    {
        double[] sides = { side1, side2, side3 };
        Array.Sort(sides);
        
        return sides;
    }
    
    private static bool IsRightTriangle(IList<double> sortedSides)
    {
        var a = sortedSides[0];
        var b = sortedSides[1];
        var c = sortedSides[2];

        return Math.Abs(a * a + b * b - c * c) < 0.000001;
    }
}