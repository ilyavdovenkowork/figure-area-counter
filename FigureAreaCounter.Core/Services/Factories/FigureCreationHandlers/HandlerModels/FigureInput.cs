﻿using FigureAreaCounter.Core.Models.Parameters.Interfaces;

namespace FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.HandlerModels;

public class FigureInput
{
    public FigureInput(string figureType, IFigureParameters parameters)
    {
        FigureType = figureType;
        Parameters = parameters;
    }

    public string FigureType { get; }
    public IFigureParameters Parameters { get; }
}