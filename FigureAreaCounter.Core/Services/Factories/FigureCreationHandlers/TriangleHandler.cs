﻿using FigureAreaCounter.Core.Models.Figures;
using FigureAreaCounter.Core.Models.Figures.Interfaces;
using FigureAreaCounter.Core.Models.Parameters;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.HandlerModels;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.Interfaces;

namespace FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers;

public class TriangleHandler : FigureHandlerBase
{
    public TriangleHandler(IFigureHandler? nextHandler) : base(nextHandler)
    {
    }

    protected override bool CanHandle(FigureInput input)
    {
        if (input.FigureType.Equals("triangle", StringComparison.OrdinalIgnoreCase) 
            && input.Parameters is TriangleParameters parameters)
        {
            return IsTriangleExists(parameters.A, parameters.B, parameters.C);
        }
        return false;
    }

    protected override IFigure HandleInternal(FigureInput input)
    {
        if (input.Parameters is not TriangleParameters parameters)
        {
            throw new ArgumentException("Invalid parameters for triangle figure");
        }
        
        var triangle = new Triangle
        {
            A = parameters.A,
            B = parameters.B,
            C = parameters.C
        };

        return triangle;
    }

    protected static bool IsTriangleExists(double sideA, double sideB, double sideC)
    {
        if (sideA <= 0 || sideB <= 0 || sideC <= 0)
        {
            throw new ArgumentException("Sides of triangle must be positive.");
        }
        
        return sideA + sideB > sideC && sideA + sideC > sideB && sideB + sideC > sideA;
    }

}