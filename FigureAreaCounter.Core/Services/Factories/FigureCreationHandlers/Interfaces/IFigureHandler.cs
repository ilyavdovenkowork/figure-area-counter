﻿using FigureAreaCounter.Core.Models.Figures.Interfaces;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.HandlerModels;

namespace FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.Interfaces;

public interface IFigureHandler
{
    IFigure Handle(FigureInput input);
}