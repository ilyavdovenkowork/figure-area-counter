﻿using FigureAreaCounter.Core.Models.Figures;
using FigureAreaCounter.Core.Models.Figures.Interfaces;
using FigureAreaCounter.Core.Models.Parameters;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.HandlerModels;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.Interfaces;

namespace FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers;

public class CircleHandler : FigureHandlerBase
{
    public CircleHandler(IFigureHandler? nextHandler) : base(nextHandler)
    {
    }

    protected override bool CanHandle(FigureInput input)
    {
        return input.FigureType.Equals("circle", StringComparison.OrdinalIgnoreCase);
    }

    protected override IFigure HandleInternal(FigureInput input)
    {
        if (input.Parameters is not CircleParameters parameters)
        {
            throw new ArgumentException("Invalid parameters for circle figure");
        }

        if (parameters.Radius <= 0)
        {
            throw new ArgumentException("Radius can not be less or equal 0.", nameof(input));
        }

        var circle = new Circle
        {
            Radius = parameters.Radius
        };

        return circle;
    }
}