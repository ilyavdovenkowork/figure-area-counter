﻿using FigureAreaCounter.Core.Models.Figures.Interfaces;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.HandlerModels;

namespace FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers;

public class EmptyHandler : FigureHandlerBase
{
    public EmptyHandler() : base(null)
    {
    }

    protected override bool CanHandle(FigureInput input)
    {
        return false;
    }

    protected override IFigure HandleInternal(FigureInput input)
    {
        throw new InvalidOperationException("Can not handle input with empty handler.");
    }
}