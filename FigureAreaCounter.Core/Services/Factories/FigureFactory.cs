﻿using FigureAreaCounter.Core.Models.Figures.Interfaces;
using FigureAreaCounter.Core.Models.Parameters.Interfaces;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.HandlerModels;
using FigureAreaCounter.Core.Services.Factories.FigureCreationHandlers.Interfaces;

namespace FigureAreaCounter.Core.Services.Factories;

public class FigureFactory
{
    private readonly IFigureHandler _handlerChain;

    public FigureFactory()
    {
        _handlerChain = new CircleHandler(new RightTriangleHandler(new TriangleHandler(new RectangleHandler(new EmptyHandler()))));
    }

    public IFigure CreateFigure(string typeName, IFigureParameters parameters)
    {
        if (string.IsNullOrEmpty(typeName))
        {
            throw new ArgumentException(string.Empty, nameof(typeName));
        }
        
        var input = new FigureInput(typeName, parameters);
        return _handlerChain.Handle(input);
    }
}