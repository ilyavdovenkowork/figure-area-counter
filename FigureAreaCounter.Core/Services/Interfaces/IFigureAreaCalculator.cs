using FigureAreaCounter.Core.Models.Figures.Interfaces;

namespace FigureAreaCounter.Core.Services.Interfaces;

public interface IFigureAreaCalculator 
{
    double CalculateArea(IFigure figure);
}