﻿using FigureAreaCounter.Core.Models.Figures;
using FigureAreaCounter.Core.Models.Figures.Interfaces;
using FigureAreaCounter.Core.Services.Interfaces;

namespace FigureAreaCounter.Core.Services;

public class FigureAreaCalculator : IFigureAreaCalculator
{
    private readonly Dictionary<Type, Func<IFigure, double>> _areaCalculators;

    public FigureAreaCalculator()
    {
        _areaCalculators = new Dictionary<Type, Func<IFigure, double>>
        {
            [typeof(Circle)] = CalculateCircleArea,
            [typeof(RightTriangle)] = CalculateRightsTriangleArea,
            [typeof(Triangle)] = CalculateTriangleArea,
            [typeof(Rectangle)] = CalculateRectangleArea
        };
    }

    public double CalculateArea(IFigure? figure)
    {
        if (figure == null)
        {
            throw new ArgumentNullException(nameof(figure));
        }
        
        if (!_areaCalculators.TryGetValue(figure.GetType(), out var calculator))
        {
            throw new ArgumentException($"Unsupported figure type: {figure.Name}");
        }

        return calculator(figure);
    }

    private static double CalculateCircleArea(IFigure figure)
    {
        var circle = (Circle)figure;
        return Math.PI * Math.Pow(circle.Radius, 2);
    }

    private static double CalculateTriangleArea(IFigure figure)
    {
        var triangle = (Triangle)figure;
        var s = (triangle.A + triangle.B + triangle.C) / 2;
        return Math.Sqrt(s * (s - triangle.A) * (s - triangle.B) * (s - triangle.C));
    }
    
    private static double CalculateRightsTriangleArea(IFigure figure)
    {
        var triangle = (RightTriangle)figure;
        return triangle.CathetusOne * triangle.CathetusTwo * 0.5;
    }

    private static double CalculateRectangleArea(IFigure figure)
    {
        var rectangle = (Rectangle)figure;
        return rectangle.Width * rectangle.Height;
    }
}